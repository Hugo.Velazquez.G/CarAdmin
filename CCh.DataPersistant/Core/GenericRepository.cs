﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using CCh.Domain.Helpers;
using CCh.Domain.IRepository;

namespace CCh.DataPersistant.Core
{
    public class GenericRepository<TEntity> : IGenericRepository<TEntity> where TEntity : class
    {
        protected readonly DbContext Context;

        protected readonly DbSet<TEntity> DbSet;

        public GenericRepository(DbContext Context)
        {
            this.Context = Context;
            this.DbSet = Context.Set<TEntity>();
        }

        public virtual void Add(List<TEntity> lstItem)
        {
            this.DbSet.AddRange(lstItem);
        }

        public virtual async Task<List<TEntity>> Get()
        {
            return await this.Get(null);
        }

        public virtual async Task<List<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> sorting = null)
        {
            var query = this.DbSet.AsQueryable();

            if (!object.Equals(null, filter))
            {
                query = query.Where(filter);
            }

            if (!object.Equals(null, sorting))
            {
                query = query.ObjectSort(sorting);
            }

            return await query.ToListAsync();
        }

        public async Task<TEntity> GetById(int id)
        {
            return await this.DbSet.FindAsync(id);
        }

        public Task<TEntity> Set(TEntity item)
        {
            throw new NotImplementedException();
        }
    }
}
