﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CCh.DataPersistant.Model;
using CCh.DataPersistant.Repository;
using CCh.Domain.IRepository;

namespace CCh.DataPersistant.Core
{
    public class UnitOfWork : IUnitOfWork
    {
        public readonly CarContext Context;

        private IRepositoryCarBrand repositoryCarBrand;

        private Dictionary<string, object> repositories;

        private Dictionary<string, object> repositoriesFor;

        public UnitOfWork(CarContext Context)
        {
            this.Context = Context;
        }

        public IRepositoryCarBrand RepositoryCar(IRepositoryCarBrand repositoryCar)
        {
            if (object.Equals(null, this.repositoryCarBrand))
            {
                this.repositoryCarBrand = new RepositoryCarBrand(this.Context);
            }

            return this.repositoryCarBrand;
        }

        public IGenericRepository<T> Repository<T>() where T : class
        {
            if (object.Equals(null, repositories))
            {
                this.repositories = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;

            if (!this.repositories.ContainsKey(type))
            {
                var repositoryInstance = Activator.CreateInstance(typeof(GenericRepository<T>), new object[] { this.Context });
                repositories.Add(type, repositoryInstance);
            }

            return (IGenericRepository<T>)this.repositories[type];
        }

        /// <summary>
        /// Método que se encarga de generar la instancia de repositorios con implementación
        /// </summary>
        /// <typeparam name="T">Tipo de repositorio</typeparam>
        /// <returns>Retorna una instancia del repositorio con implementación</returns>
        public T RepositoryFor<T>() where T : class
        {
            if (this.repositoriesFor == null)
            {
                this.repositoriesFor = new Dictionary<string, object>();
            }

            var type = typeof(T).Name;
            var repoName = type.Substring(1);
            var repositoryType = AppDomain.CurrentDomain.GetAssemblies().SelectMany(x => x.GetTypes()).First(x => x.Name == repoName);

            if (!this.repositoriesFor.ContainsKey(type))
            {
                var repositoryInstance = (T)Activator.CreateInstance(repositoryType, new object[] { this.Context });
                this.repositoriesFor.Add(type, repositoryInstance);
            }

            return (T)this.repositoriesFor[type];
        }

        public bool SaveContext()
        {
            return this.Context.SaveChanges() > 0;
        }

        public async Task<bool> SaveContextAsync()
        {
            return await this.Context.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(this);
        }
    }
}
