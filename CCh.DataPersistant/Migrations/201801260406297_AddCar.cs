namespace CCh.DataPersistant.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCar : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.Cars", "CarLikeUnlikeId", "dbo.CarLikeUnlikes");
            DropIndex("dbo.Cars", new[] { "CarLikeUnlikeId" });
            AlterColumn("dbo.Cars", "CarLikeUnlikeId", c => c.Int());
            CreateIndex("dbo.Cars", "CarLikeUnlikeId");
            AddForeignKey("dbo.Cars", "CarLikeUnlikeId", "dbo.CarLikeUnlikes", "CarLikeUnlikeId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Cars", "CarLikeUnlikeId", "dbo.CarLikeUnlikes");
            DropIndex("dbo.Cars", new[] { "CarLikeUnlikeId" });
            AlterColumn("dbo.Cars", "CarLikeUnlikeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Cars", "CarLikeUnlikeId");
            AddForeignKey("dbo.Cars", "CarLikeUnlikeId", "dbo.CarLikeUnlikes", "CarLikeUnlikeId", cascadeDelete: true);
        }
    }
}
