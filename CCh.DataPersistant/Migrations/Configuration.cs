namespace CCh.DataPersistant.Migrations
{
    using System.Data.Entity.Migrations;

    internal sealed class Configuration : DbMigrationsConfiguration<CCh.DataPersistant.Model.CarContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(CCh.DataPersistant.Model.CarContext context)
        {
            /// TODO: 
            ///  This method will be called after migrating to the latest version.

            ///  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            ///  to avoid creating duplicate seed data.
        }
    }
}
