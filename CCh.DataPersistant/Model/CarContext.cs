﻿using System.Data.Entity;
using CCh.Domain.Entity;

namespace CCh.DataPersistant.Model
{
    public class CarContext : DbContext
    {
        public DbSet<CarBrand> CarBrands { get; set; }
        public DbSet<CarType> CarTypes { get; set; }
        public DbSet<CarLikeUnlike> Likes { get; set; }
        public DbSet<Car> Cars { get; set; }
    }
}
