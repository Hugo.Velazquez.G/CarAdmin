﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using CCh.DataPersistant.Core;
using CCh.DataPersistant.Model;
using CCh.Domain.Dto;
using CCh.Domain.Entity;
using CCh.Domain.IRepository;

namespace CCh.DataPersistant.Repository
{
    public class RepositoryCarBrand : GenericRepository<CarBrand>, IRepositoryCarBrand
    {
        private readonly CarContext Context;

        public RepositoryCarBrand(CarContext context) : base(context)
        {
            this.Context = context;
        }

        public async Task<List<BrandDto>> GetGroupByBrand()
        {
            var query = this.Context.CarBrands.AsQueryable();

            var grouping = from car in query
                   group car by new { car.CarBrandId, car.Name } into g
                   select new BrandDto
                   {
                       BrandId = g.Key.CarBrandId,
                       BrandName = g.Key.Name,
                       Cars = g.Select(c => c.Cars).FirstOrDefault()
                   };

            return await grouping.ToListAsync();
        }
    }
}
