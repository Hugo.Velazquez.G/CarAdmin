﻿using System.Collections.Generic;
using CCh.Domain.Entity;

namespace CCh.Domain.Dto
{
    public class BrandDto
    {
        public int BrandId { get; set; }
        public string BrandName { get; set; }
        public List<Car> Cars { get; set; }
    }
}
