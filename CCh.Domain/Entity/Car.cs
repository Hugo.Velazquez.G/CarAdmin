﻿namespace CCh.Domain.Entity
{
    public class Car
    {
        public int CarId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        
        public int? CarLikeUnlikeId { get; set; }
        public virtual CarLikeUnlike Like {get; set;}

        public int CarBrandId { get; set; }
        public virtual CarBrand Brand { get; set; }

        public int CarTypeId { get; set; }
        public virtual CarType Type { get; set; }
    }
}
