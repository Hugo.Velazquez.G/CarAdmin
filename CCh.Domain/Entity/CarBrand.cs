﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CCh.Domain.Entity
{
    public class CarBrand
    {
        public int CarBrandId { get; set; }
        public string Name { get; set; }

        [JsonIgnore] 
        public virtual List<Car> Cars { get; set; }
    }
}
