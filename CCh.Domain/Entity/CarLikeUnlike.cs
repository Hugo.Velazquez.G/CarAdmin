﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CCh.Domain.Entity
{
    public class CarLikeUnlike
    {
        public int CarLikeUnlikeId { get; set; }
        public int Like { get; set; }
        public int Unlike { get; set; }

        [JsonIgnore]
        public virtual List<Car> Cars { get; set; }
    }
}
