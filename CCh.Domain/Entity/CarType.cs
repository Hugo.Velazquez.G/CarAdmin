﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace CCh.Domain.Entity
{
    public class CarType
    {
        public int CarTypeId { get; set; }
        public string Name { get; set; }

        [JsonIgnore]
        public virtual List<Car> Cars { get; set; }
    }
}
