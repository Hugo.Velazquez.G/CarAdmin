﻿using Newtonsoft.Json;

namespace CCh.Domain.Helpers
{
    public static class Serializer
    {
        public static string ObjectToJson<T>(this T item) where T : class, new()
        {
            return JsonConvert.SerializeObject(item);
        }
    }
}
