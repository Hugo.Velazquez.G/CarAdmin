﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace CCh.Domain.IRepository
{
    public interface IGenericRepository<TEntity> where TEntity : class
    {
        /// <summary>
        /// Method to gets a <see cref="List"/> of <see cref="TEntity"/>
        /// </summary>
        /// <returns><see cref="List{TEntity}"/></returns>
        Task<List<TEntity>> Get();

        /// <summary>
        /// Method to gets a <see cref="List"/> of <see cref="TEntity"/>
        /// </summary>
        /// <param name="filter">Contains a filter</param>
        /// <returns><see cref="List{TEntity}"/></returns>
        Task<List<TEntity>> Get(Expression<Func<TEntity, bool>> filter = null, Expression<Func<TEntity, object>> sorting = null);

        /// <summary>
        /// Method to search a register using ID
        /// </summary>
        /// <returns><see cref="TEntity"/></returns>
        Task<TEntity> GetById(int id);

        /// <summary>
        /// Method to add a register
        /// </summary>
        /// <param name="lstItem">List <see cref="List{TEntity}"/> to be updated</param>
        /// <param name="item">Item whit the information</param>
        void Add(List<TEntity> item);

        /// <summary>
        /// Method to update a Row
        /// </summary>
        /// <param name="Item">Object <see cref="TEntity"/> to be updated</param>
        /// <returns><see cref="bool"/></returns>
        Task<TEntity> Set(TEntity item);
    }
}
