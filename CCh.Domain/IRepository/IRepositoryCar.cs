﻿using CCh.Domain.Entity;

namespace CCh.Domain.IRepository
{
    public interface IRepositoryCar : IGenericRepository<Car>
    {
    }
}
