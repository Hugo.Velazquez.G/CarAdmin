﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CCh.Domain.Dto;
using CCh.Domain.Entity;

namespace CCh.Domain.IRepository
{
    public interface IRepositoryCarBrand : IGenericRepository<CarBrand>
    {
        /// <summary>
        /// Get car List by brands
        /// </summary>
        /// <returns>Return <see cref="BrandDto"/> List</returns>
        Task<List<BrandDto>> GetGroupByBrand();
    }
}
