﻿using CCh.Domain.Entity;

namespace CCh.Domain.IRepository
{
    public interface IRepositoryCarLikeUnlike : IGenericRepository<CarLikeUnlike>
    {
    }
}
