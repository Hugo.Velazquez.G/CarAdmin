﻿using CCh.Domain.Entity;

namespace CCh.Domain.IRepository
{
    public interface IRepositoryCarType : IGenericRepository<CarType>
    {
    }
}
