﻿using System;
using System.Threading.Tasks;

namespace CCh.Domain.IRepository
{
    public interface IUnitOfWork : IDisposable
    {
        IGenericRepository<T> Repository<T>() where T : class;

        T RepositoryFor<T>() where T : class;

        bool SaveContext();

        Task<bool> SaveContextAsync();
    }
}
