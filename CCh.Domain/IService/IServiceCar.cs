﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CCh.Domain.Entity;

namespace CCh.Domain.IService
{
    public interface IServiceCar
    {
        Task<List<Car>> Get(Car item);
        Task<string> GetJson(Car item);
        Task<Car> GetById(int id);
        Task<CarLikeUnlike> SetVote(CarLikeUnlike item);
    }
}
