﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CCh.Domain.Dto;

namespace CCh.Domain.IService
{
    public interface IServiceCarBrand
    {
        Task<List<BrandDto>> GetGroup();
    }
}
