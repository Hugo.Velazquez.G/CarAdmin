﻿using System.Threading.Tasks;
using CCh.Domain.Entity;

namespace CCh.Domain.IService
{
    public interface IServiceCarLikeUnlike
    {
        Task<CarLikeUnlike> SetLikes(CarLikeUnlike item);
    }
}
