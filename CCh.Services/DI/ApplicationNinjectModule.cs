﻿using CCh.DataPersistant.Core;
using CCh.Domain.IRepository;
using Ninject.Modules;

namespace CCh.Services.DI
{
    public class ApplicationNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>();
        }
    }
}
