﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CCh.Domain.Entity;
using CCh.Domain.Helpers;
using CCh.Domain.IRepository;
using CCh.Domain.IService;

namespace CCh.Services.Services
{
    public class ServiceCar : IServiceCar
    {
        private IUnitOfWork UnitOfWork;

        public ServiceCar(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public async Task<List<Car>> Get(Car item)
        {
            var result = await this.UnitOfWork.Repository<Car>().Get(x => x.Brand.Name.Contains(item.Brand.Name), x => x.Like.Like);
            return result;
        }

        public async Task<string> GetJson(Car item)
        {
            var result = await this.UnitOfWork.Repository<Car>().Get(x => x.Brand.Name.Contains(item.Brand.Name), x => x.Like.Like);

            return result.ObjectToJson();
        }

        public async Task<Car> GetById(int id)
        {
            var result = await this.UnitOfWork.Repository<Car>().GetById(id);
            return result;
        }

        public async Task<CarLikeUnlike> SetVote(CarLikeUnlike item)
        {
            var result = await this.UnitOfWork.Repository<CarLikeUnlike>().Set(item);
            await this.UnitOfWork.SaveContextAsync();

            return result;
        }
    }
}
