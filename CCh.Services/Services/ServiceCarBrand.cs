﻿using System.Collections.Generic;
using System.Threading.Tasks;
using CCh.Domain.Dto;
using CCh.Domain.IRepository;
using CCh.Domain.IService;

namespace CCh.Services.Services
{
    public class ServiceCarBrand : IServiceCarBrand
    {
        private IUnitOfWork UnitOfWork;

        public ServiceCarBrand(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public async Task<List<BrandDto>> GetGroup()
        {
            return await this.UnitOfWork.RepositoryFor<IRepositoryCarBrand>().GetGroupByBrand();
        }
    }
}
