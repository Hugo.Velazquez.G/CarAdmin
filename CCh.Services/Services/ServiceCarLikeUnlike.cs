﻿using System.Threading.Tasks;
using CCh.Domain.Entity;
using CCh.Domain.IRepository;
using CCh.Domain.IService;

namespace CCh.Services.Services
{
    public class ServiceCarLikeUnlike : IServiceCarLikeUnlike
    {
        private IUnitOfWork UnitOfWork;

        public ServiceCarLikeUnlike(IUnitOfWork unitOfWork)
        {
            this.UnitOfWork = unitOfWork;
        }

        public async Task<CarLikeUnlike> SetLikes(CarLikeUnlike item)
        {
            await this.UnitOfWork.Repository<CarLikeUnlike>().Set(item);

            var result = await this.UnitOfWork.SaveContextAsync();

            if (!result)
                return null;

            return item;
        }
    }
}
