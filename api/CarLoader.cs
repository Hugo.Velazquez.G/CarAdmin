﻿using System;

//using api.Models;

namespace api
{
    public static class CarLoader
    {
        public static void LoadCars()
        {
            LoadCar("Ford", CarsResource.ford);
            LoadCar("Ferrari", CarsResource.ferrari);
            LoadCar("Nissan", CarsResource.nissan);
            LoadCar("Toyota", CarsResource.toyota);
        }

        private static void LoadCar(string car, string resource)
        {
            var list = resource.Split(new[] { '\n' }, StringSplitOptions.RemoveEmptyEntries);
            foreach(var image in list)
            {
                //TODO: load data into object context
            }
        }
    }
}