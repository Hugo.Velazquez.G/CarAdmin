﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using CCh.Domain.IService;

namespace api.Controllers
{
    /// <summary>
    /// Controller with car brand operation
    /// </summary>
    public class CarBrandController : ApiController
    {
        /// <summary>
        /// Service Car Brand interface
        /// </summary>
        private IServiceCarBrand ServiceCarBrand;

        /// <summary>
        /// Car brand Controller constructor
        /// </summary>
        /// <param name="serviceCarBrand">Car Brand interface</param>
        public CarBrandController(IServiceCarBrand serviceCarBrand)
        {
            this.ServiceCarBrand = serviceCarBrand;
        }

        /// <summary>
        /// Method to get Cars Grouped by brand
        /// </summary>
        /// <returns><see cref="BrandDto"/> List</returns>
        public async Task<IHttpActionResult> Get()
        {
            try
            {
                var result = await this.ServiceCarBrand.GetGroup();

                return this.Ok(result);
            }
            catch (Exception ex)
            {
                return this.InternalServerError(ex);
            }
        }
    }
}
