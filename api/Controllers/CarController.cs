﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using CCh.Domain.Entity;
using CCh.Domain.IService;

namespace api.Controllers
{
    /// <summary>
    /// Controller with car operations
    /// </summary>
    public class CarController : ApiController
    {
        /// <summary>
        /// Service Car Inteface
        /// </summary>
        private IServiceCar ServiceCar;

        /// <summary>
        /// Car Controller constructor
        /// </summary>
        /// <param name="serviceCar">Service Car Inteface</param>
        public CarController(IServiceCar serviceCar)
        {
            this.ServiceCar = serviceCar;
        }

        /// <summary>
        /// Method to get Cars by Brand
        /// </summary>
        /// <param name="brand">Brand <see cref="string"/></param>
        /// <returns>Returns <see cref="Car"/> list</returns>
        public async Task<IHttpActionResult> Get(string brand)
        {
            try
            {
                if (string.IsNullOrEmpty(brand))
                {
                    return this.BadRequest("I'm Sorry, To get Cars by brand you need a brand :S, please entry one");
                }

                var item = new Car
                {
                    Brand = new CarBrand
                    {
                        Name = brand
                    }
                };

                var result = await this.ServiceCar.Get(item);

                return this.Ok(result);
            }
            catch (Exception ex)
            {
                this.InternalServerError(ex);
            }

            throw new NotImplementedException();
        }

        /// <summary>
        /// Method to get Cars by Id
        /// </summary>
        /// <param name="id">Car Identifier</param>
        /// <returns>Returns a <see cref="Car"/> object.</returns>
        public async Task<IHttpActionResult> Get(int id)
        {
            try
            {
                if (id <= 0)
                {
                    return this.BadRequest("I'm Sorry, To get Cars by Id you need an Id :S, please entry one");
                }

                var result = await this.ServiceCar.GetById(id);

                return this.Ok(result);
            }
            catch (Exception ex)
            {
                return this.InternalServerError(ex);
            }
        }
    }
}
