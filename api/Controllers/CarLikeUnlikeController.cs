﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using CCh.Domain.Entity;
using CCh.Domain.IService;

namespace api.Controllers
{
    public class CarLikeUnlikeController : ApiController
    {
        private IServiceCarLikeUnlike ServiceCarLikeUnlike;

        public CarLikeUnlikeController(IServiceCarLikeUnlike serviceCarLikeUnlike)
        {
            this.ServiceCarLikeUnlike = serviceCarLikeUnlike;
        }

        /// <summary>
        /// method to update likes and unlikes
        /// </summary>
        /// <param name="item">Contains information to update</param>
        public async Task<IHttpActionResult> Post([FromBody]Car item)
        {
            try
            {
                if (object.Equals(null, item))
                {
                    return this.BadRequest("I'm Sorry, To update Likes you need a like object :S, please entry one");
                }

                if (item.CarId <= 0 || item.Like.Like < 0 || item.Like.Unlike < 0 || item.CarLikeUnlikeId <= 0)
                {
                    return this.BadRequest("I'm Sorry we're not able to make update, please entry one");
                }

                var carLikeUnlike = new CarLikeUnlike
                {
                    CarLikeUnlikeId = item.CarLikeUnlikeId.Value,
                    Like = item.Like.Like,
                    Unlike = item.Like.Unlike
                };

                var result = await this.ServiceCarLikeUnlike.SetLikes(carLikeUnlike);

                return this.Ok(result);
            }
            catch (Exception ex)
            {

                return this.InternalServerError(ex);
            }
        }
    }
}
