﻿using CCh.Domain.IService;
using CCh.Services.Services;
using Ninject.Modules;

namespace api.DI
{
    public class ApiNinjectModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IServiceCar>().To<ServiceCar>();
            Bind<IServiceCarBrand>().To<ServiceCarBrand>();
            Bind<IServiceCarLikeUnlike>().To<ServiceCarLikeUnlike>();
        }
    }
}