﻿using CCh.Services.DI;
using Ninject;

namespace api.DI
{
    public class IoC
    {
        public IKernel Kernel { get; set; }

        public IoC()
        {
            this.Kernel = this.GetNinjectModules();
        }

        public StandardKernel GetNinjectModules()
        {
            return new StandardKernel(
                new ApiNinjectModule(), 
                new ApplicationNinjectModule());
        }
    }
}